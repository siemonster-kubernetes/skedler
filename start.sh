#!/bin/bash
# Restore mounted volumes
if [ -z "$(ls -A /root/.local/share/data/bconf)" ]; then
    cp -a /.backup/skedler/license/* /root/.local/share/data/bconf/
fi
if [ -z "$(ls -A /data/)" ]; then
    cp -aR /.backup/skedler/data/* /data/
fi
if [ -z "$(ls -A /opt/skedler/config)" ]; then
    cp -aR /.backup/skedler/config/* /opt/skedler/config/
fi
## SKEDLER PWD
#sed -ri "s|skedler_elasticsearch_password:[^\r\n]*|skedler_elasticsearch_password: $ELASTIC_PWD|" /opt/skedler/config/reporting.yml
#sed -ri "s|skedler_elasticsearch_username:[^\r\n]*|skedler_elasticsearch_username: $ELASTIC_USER|" /opt/skedler/config/reporting.yml
#sed -ri "s|skedler_grafana_password:[^\r\n]*|skedler_grafana_password: $GF_SECURITY_ADMIN_PASSWORD|" /opt/skedler/config/reporting.yml
#sed -ri "s|skedler_grafana_username:[^\r\n]*|skedler_grafana_username: $GF_SECURITY_ADMIN_USER|" /opt/skedler/config/reporting.yml
echo -e "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts
/opt/skedler/bin/skedler
